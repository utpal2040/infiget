import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';

import { SuperTabsModule } from 'ionic2-super-tabs';


import { Data } from '../providers/data';
import { MyApp } from './app.component';


import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { RetailerSignup } from '../pages/retailersignup/retailersignup' 
import { SplashscreenPage } from '../pages/splashscreen/splashscreen' 
import { MidconnectorPage } from '../pages/midconnector/midconnector' 
import { ProductcatsubcatPage } from '../pages/productcatsubcat/productcatsubcat' 
import { ProductdetailsPage } from '../pages/productdetails/productdetails'
import { CartdetailsPage } from '../pages/cartdetails/cartdetails'

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

//import { Ng2SearchPipeModule } from 'ng2-search-filter';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    RetailerSignup,
    SplashscreenPage,
    MidconnectorPage,
    ProductcatsubcatPage,
    ProductdetailsPage,
    CartdetailsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    //Ng2SearchPipeModule,
    IonicStorageModule.forRoot(),
    SuperTabsModule.forRoot()
  
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    RetailerSignup,
    SplashscreenPage,
    MidconnectorPage,
    ProductcatsubcatPage,
    ProductdetailsPage,
    CartdetailsPage

  ],
  providers: [
    Data,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
  
})
export class AppModule {}
