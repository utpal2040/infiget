import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {Storage} from '@ionic/storage';
import { ListPage } from '../pages/list/list';
import { SplashscreenPage } from '../pages/splashscreen/splashscreen' 


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = SplashscreenPage;

  pages: Array<{title: string, icon: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,private storage:Storage
  ) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
       { title: 'Home', icon: 'home', component: ListPage },
       { title: 'Schemes', icon: 'log-out', component: ListPage },
       { title: 'Track your order', icon: '', component: ListPage },
       { title: 'Favourite Items', icon: 'heart', component: ListPage },
       { title: 'Reports', icon: '', component: ListPage },
       { title: 'My Cart', icon: 'cart', component: ListPage },
        { title: 'Log Out', icon: 'log-out', component: null }
     
    
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    if(page.component) {
        this.nav.setRoot(page.component);
       }else{
         this.storage.set('logindata', null); 
        this.nav.setRoot(SplashscreenPage);
    }
  }
}
