import { Component } from '@angular/core';
import { NavController,NavParams,MenuController,ToastController} from 'ionic-angular';

import {SuperTabsController} from "ionic2-super-tabs";

import { ProductdetailsPage } from '../productdetails/productdetails';


@Component({
  selector: 'page-productcatsubcat',
  templateUrl: 'productcatsubcat.html',
})
export class ProductcatsubcatPage {

  page1: any = 'TabmobilePage';
  page2: any = 'TabPageAccessoryPage';
  page3: any = 'TabPageTabletPage';

  showIcons: boolean = true;
  showTitles: boolean = true;
  pageTitle: string = 'Full Height';

  constructor(public navCtrl: NavController,private navParams: NavParams, public menuCtrl: MenuController,public toastCtrl: ToastController, private superTabsCtrl: SuperTabsController) {
   this.menuCtrl.enable(true, 'myMenu');

   const type = navParams.get('type');
    switch (type) {
      case 'icons-only':
        this.showTitles = false;
        this.pageTitle += ' - Icons only';
        break;

      case 'titles-only':
        this.showIcons = false;
        this.pageTitle += ' - Titles only';
        break;
    }
  }
  
 midconnector(){
   let toast = this.toastCtrl.create({
      message: 'This functionality is Under Construction',
      duration: 3000
      });
      toast.present();

 }

 onTabSelect(tab: { index: number; id: string; }) {
    console.log(`Selected tab: `, tab);
  }
 
}
