import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,MenuController} from 'ionic-angular';
import { ProductcatsubcatPage } from '../productcatsubcat/productcatsubcat'; 

@Component({
  selector: 'page-midconnector',
  templateUrl: 'midconnector.html',
})
export class MidconnectorPage {

  constructor(public navCtrl: NavController, public menuCtrl: MenuController) {
    this.menuCtrl.enable(true, 'myMenu');
  }

  productcatsubcat(){
    this.navCtrl.push(ProductcatsubcatPage, {})   

  }

}
