import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProductdetailsPage } from '../productdetails/productdetails';

/**
 * Generated class for the TabmobilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tabmobile',
  templateUrl: 'tabmobile.html',
})
export class TabmobilePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabmobilePage');
  }

  productdetails(){
  	this.navCtrl.push(ProductdetailsPage, {})
  }

}
