import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabPageAccessoryPage } from './tab-page-accessory';

@NgModule({
  declarations: [
    TabPageAccessoryPage,
  ],
  imports: [
    IonicPageModule.forChild(TabPageAccessoryPage),
  ],
})
export class TabPageAccessoryPageModule {}
