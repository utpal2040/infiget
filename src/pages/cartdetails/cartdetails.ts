import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-cartdetails',
  templateUrl: 'cartdetails.html',
})
export class CartdetailsPage {

	public amount:number=1;

  constructor(public navCtrl: NavController, public navParams: NavParams,public menuCtrl: MenuController) {
  	this.menuCtrl.enable(true, 'myMenu');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CartdetailsPage');
  }

  addAmount(){
  	this.amount=this.amount+1;
  }

  removeAmount(){
  	if (this.amount === 0){
  		this.amount=0;
  	}
  	else{
  		this.amount=this.amount-1;
  	}
  }

}
