import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CartdetailsPage } from './cartdetails';

@NgModule({
  declarations: [
    CartdetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(CartdetailsPage),
  ],
})
export class CartdetailsPageModule {}
