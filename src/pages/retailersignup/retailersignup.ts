
import { Component } from '@angular/core';
import { NavController,MenuController,ToastController  } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';  
import { HomePage } from '../home/home'; 
import { LoadingController } from 'ionic-angular';


@Component({
   selector: 'page-retailersignup',
  templateUrl: 'retailersignup.html',
})
export class RetailerSignup {

  public branddetails: any[];
  public states: any[];
  public districts: any[];
  public cities: any[];
  
  public selectedDistricts: any[];
  public selectedCities: any[];
  
  public sState: any;
  public sDistrict: any;
  public sCity: any;

  public id_branddetails: any;
   
 public retailer_type: any[];
 public sRetailerType: any;
  
  public firstname: string;
  public lastname: string;
  public company_name: string;
  public username: string;
 
  public unique_code: string;
  public contact_name: string;
  public contact_phone: string;
  public password: string;
  public email: string;
  public address1: string;
  public address2: string;  
  public area: string;
  public pincode: string;

  

  constructor(public navCtrl: NavController, public menuCtrl: MenuController, public http: Http,public toastCtrl: ToastController,public loadingCtrl: LoadingController ){
      this.menuCtrl.enable(false, 'myMenu');
      this.initializeState();
      //this.initializeDistrict();
      this.initializeCity();
      this.initializeRetailerType();
      this.brandbydistributorcode();
   }  

  initializeRetailerType(){
        this.retailer_type = [
          {id: 1, name: 'Retailer 1'},
          {id: 2, name: 'Retailer 2'},
          {id: 3, name: 'Retailer 3'}
        ];
      }
    initializeState(){
      this.states = [
        {id: 1, name: 'Delhi'},
        {id: 2, name: 'West Bengal'},
        {id: 3, name: 'Maharastra'}
      ];
    }


    initializeCity(){
      this.cities = [
        {id: 1, name: 'Delhi Cantonment', state_id: 1},
        {id: 2, name: 'Fatehpur Beri', state_id: 1},
        {id: 3, name: 'Kolkata', state_id: 2},
        {id: 4, name: 'Asansol', state_id: 2},
        {id: 5, name: 'North Bengal', state_id: 2},
        {id: 6, name: 'Mumbai', state_id: 3},
        {id: 7, name: 'Pune', state_id: 3} 
      ];
    }

 setDistrictValues(sState) {
     this.selectedDistricts = this.districts.filter(district => district.state_id == sState.id)
 }

 setCityValues(sState) {
      this.selectedCities = this.cities.filter(city => city.state_id == sState.id);
 }

   
  goBack() {
    this.navCtrl.pop();
  }

  doRegister(){
    //alert(this.id_branddetails); 

    if(this.firstname == null || this.firstname ==""){
      let toast = this.toastCtrl.create({
      message: 'Please fill a the Firstname',
      duration: 3000
      });
      toast.present();

    }else if(this.lastname == null || this.lastname == ""){
       let toast = this.toastCtrl.create({
      message: 'Please fill the Lastname',
      duration: 3000
      });
      toast.present();

    }else if(this.company_name == null || this.company_name == ""){
       let toast = this.toastCtrl.create({
      message: 'Please fill  the Company Name',
      duration: 3000
      });
      toast.present();

    }else if(this.email == null || this.email == ""){
       let toast = this.toastCtrl.create({
      message: 'Please fill Email',
      duration: 3000
      });
      toast.present();

    }else if(this.username == null || this.username == ""){
       let toast = this.toastCtrl.create({
      message: 'Please fill Username',
      duration: 3000
      });
      toast.present();

    }else if(this.password == null || this.password == ""){
       let toast = this.toastCtrl.create({
      message: 'Please fill Password',
      duration: 3000
      });
      toast.present();

    }else if(this.address1 == null || this.address1 == ""){
       let toast = this.toastCtrl.create({
      message: 'Please fill Address1',
      duration: 3000
      });
      toast.present();

    }else if(this.area == null || this.area == ""){
       let toast = this.toastCtrl.create({
      message: 'Please fill all the Fields',
      duration: 3000
      });
      toast.present();

    }else if(this.pincode == null || this.pincode == ""){
     let toast = this.toastCtrl.create({
      message: 'Please fill  the Pincode',
      duration: 3000
      });
      toast.present();

    }else if(this.unique_code == null || this.unique_code == ""){
      let toast = this.toastCtrl.create({
      message: 'Please fill Unique',
      duration: 3000
      });
      toast.present();

    }else{
      let loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present();
      let headers = new Headers();
      headers.append('Content-Type','application/x-www-form-urlencoded; charset=UTF-8');
      let options = new RequestOptions({ headers: headers });
      
        //alert(this.sRetailerType.name);

        let reqParam = {
          first_name: this.firstname,
          last_name: this.lastname,
          companyfirm: this.company_name,
          user_type: this.sRetailerType.id,
          email: this.email,
          user_name: this.username,
          password: this.password,
          address1: this.address1,
          address2: this.address2,
          area_zone: this.area,
          city: this.sCity.id,
          state: this.sState.id,
          pincode: this.pincode,
          distributor_code : this.unique_code,
          brand_id : this.id_branddetails
      };

      console.log("response == "+JSON.stringify(reqParam));

     this.http.post("http://infiget.com/retailerwebservice/retailersignup", JSON.stringify(reqParam), options)
        .subscribe(data => {
          console.log(data['_body']);
        var response = JSON.parse(data['_body']);
        console.log(response.success);
        var success = response.success;
        if(success == 1){
           
           this.navCtrl.push(HomePage, {})   
           let toast = this.toastCtrl.create({
          message: 'Successfully Register : wait for Admin cofirmation' ,
          duration: 3000
          });
          toast.present();
          loader.dismiss();     
        }else{
          let toast = this.toastCtrl.create({
          message: 'Sorry! Unable to Register' ,
          duration: 3000
          });
          toast.present();
          loader.dismiss();
        }
         }, error => {
          console.log(error);// Error getting the data
        });

      }

    }

  brandbydistributorcode(){

    if(this.unique_code){
      if(this.unique_code.length === 6 || this.unique_code.length > 6){
        let headers = new Headers();
        headers.append('Content-Type','application/x-www-form-urlencoded; charset=UTF-8');
        let options = new RequestOptions({ headers: headers });
        let reqParam = {
          distributor_code : this.unique_code
        }
        this.http.post("http://infiget.com/api/api/brandbydistributorcode", JSON.stringify(reqParam), options)
        .subscribe(data => {
          var response = JSON.parse(data['_body']);
          if(response.success == 1){
            this.branddetails = response.branddetails;
          }
        });
      } else {
        console.log("Enter 6 digit");
      }
    } else {
      console.log("Enter distributor code");
    }
  }



} 


  