import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabPageTabletPage } from './tabpagetablet';

@NgModule({
  declarations: [
    TabPageTabletPage,
  ],
  imports: [
    IonicPageModule.forChild(TabPageTabletPage),
  ],
})
export class TabPageTabletPageModule {}
