webpackJsonp([4],{

/***/ 134:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__retailersignup_retailersignup__ = __webpack_require__(267);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__list_list__ = __webpack_require__(74);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var HomePage = (function () {
    function HomePage(navCtrl, menuCtrl, http, storage, toastCtrl) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.http = http;
        this.storage = storage;
        this.toastCtrl = toastCtrl;
        this.menuCtrl.enable(false, 'myMenu');
    }
    HomePage.prototype.loginUser = function () {
        var _this = this;
        var password = this.password;
        var username = this.username;
        if (username == '' || password == '' || username == null || password == null) {
            var toast = this.toastCtrl.create({
                message: 'Oppps! Please fill all the Fields',
                duration: 3000
            });
            toast.present();
        }
        else {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
            var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
            var postParams = {
                username: username,
                password: password
            };
            this.http.post("http://infiget.com/loginwebservice/login", JSON.stringify(postParams), options)
                .subscribe(function (data) {
                var response = JSON.parse(data['_body']);
                if (response.success == 0) {
                    var toast = _this.toastCtrl.create({
                        message: response.msg,
                        duration: 3000
                    });
                    toast.present();
                }
                else {
                    _this.storage.set('logindata', response);
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__list_list__["a" /* ListPage */], {});
                }
            }, function (error) {
                console.log(error); // Error getting the data
            });
        }
    };
    HomePage.prototype.newRegistration = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__retailersignup_retailersignup__["a" /* RetailerSignup */], {});
    };
    HomePage.prototype.underconstruction = function () {
        var toast = this.toastCtrl.create({
            message: 'This functionality is Under Construction',
            duration: 3000
        });
        toast.present();
    };
    HomePage.prototype.welcome = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__list_list__["a" /* ListPage */], {});
    };
    return HomePage;
}());
HomePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-home',template:/*ion-inline-start:"D:\suhrit\suhrit\ionic2\Fairtail\src\pages\home\home.html"*/'<ion-header>\n\n</ion-header>\n\n\n<ion-content padding class="masters">\n <ion-item no-lines class="masters">\n    <ion-avatar item-left>\n    </ion-avatar>\n    </ion-item>\n\n<ion-item no-lines class="masters">\n    <ion-row>\n   <ion-col col-6>\n    <img src="assets/img/index2.jpg"  style=" max-width: 100%;max-height: 44%;"alt="your image">\n   </ion-col>\n  <ion-col col-6>\n    <h1 color="white"> <b>Infiget</b> </h1>\n  </ion-col>\n</ion-row>\n\n </ion-item>\n\n      <ion-item class="masters">\n        <ion-label floating><b class="whitecc"> Username </b></ion-label>\n        <ion-icon item-left class="ioconcc" name="contact"> </ion-icon>\n        <ion-input type="text" class="inputfield" [(ngModel)]="username"></ion-input>\n      </ion-item>\n      \n       <br>\n     \n      <ion-item  class="masters">\n        <ion-label floating class><b  class="whitecc">Password</b></ion-label>\n        <ion-icon item-left class="ioconcc" name="lock" ></ion-icon>\n        <ion-input type="password" class="inputfield" [(ngModel)]="password"></ion-input>\n      </ion-item>\n      \n       <br>\n   \n       <button ion-button  full  color="secondary" (click)="loginUser()" class="loginbutton" >LOGIN</button>\n    \n       <ion-item no-lines class="masters">\n      <ion-row>\n        <button ion-button class="txtcentre" (click)="underconstruction()" clear small>Forget Password?</button>\n      </ion-row>\n\n      <ion-row >\n        <button ion-button class="txtcentre" (click)="newRegistration()" clear small>New Registration</button>\n      </ion-row>\n\n       </ion-item>\n\n  </ion-content>\n'/*ion-inline-end:"D:\suhrit\suhrit\ionic2\Fairtail\src\pages\home\home.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["n" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["l" /* MenuController */], __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["s" /* ToastController */]])
], HomePage);

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 135:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductdetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProductdetailsPage = (function () {
    function ProductdetailsPage(navCtrl, navParams, menuCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.menuCtrl = menuCtrl;
        this.menuCtrl.enable(true, 'myMenu');
    }
    ProductdetailsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProductdetailsPage');
    };
    return ProductdetailsPage;
}());
ProductdetailsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-productdetails',template:/*ion-inline-start:"D:\suhrit\suhrit\ionic2\Fairtail\src\pages\productdetails\productdetails.html"*/'<ion-header  >\n   <ion-toolbar>\n   <button ion-button menuToggle>\n      <ion-icon name="menu" class="whitecc"></ion-icon>\n    </button>\n    <ion-title ><b class="whitecc"> Infiget India  Pvt. </b> <ion-icon class="whitecc" style="margin-left: 1cm;"  name="cart"></ion-icon></ion-title>\n     \n   </ion-toolbar>\n\n<ion-toolbar>\n<ion-searchbar ></ion-searchbar>\n<ion-list>\n \n</ion-list>\n   </ion-toolbar>\n\n\n</ion-header>\n\n<ion-content style="background-color: #E9ECF3; padding-right: 20px;">\n	<ion-card>\n		<ion-card-content>\n 			<ion-row >\n  			<ion-col col-md-12 style="padding-bottom: 50px;"> \n   			<img class="center" src="assets/mobile/honor_8_pro_screen_small_1497260751475.jpg"/>\n  			</ion-col>\n			</ion-row>\n			<ion-row>\n				<ion-col col-md-4 class="border" style="padding: 0px;">\n					<img src="assets/mobile/honor_8_pro_screen_small_1497260751475.jpg" class="custom-avatar"/>\n				</ion-col>\n				<ion-col col-md-4 class="border" style="padding: 0px;">\n					<img src="assets/mobile/honor_8_pro_screen_small_1497260751475.jpg" class="custom-avatar"/>\n				</ion-col>\n				<ion-col col-md-4 class="border" style="padding: 0px;">\n					<img src="assets/mobile/honor_8_pro_screen_small_1497260751475.jpg" class="custom-avatar"/>\n				</ion-col>\n			</ion-row>\n			<ion-row>\n				<ion-col col-md-12>\n					<h1 style="padding-bottom: 10px;">Specification</h1>\n					<p style="padding-bottom: 10px;"><b>Storage:</b>32GB</p>\n					<p style="padding-bottom: 10px;"><b>Battery:</b>4100mAh</p>\n					<p style="padding-bottom: 10px;"><b>Warranty:</b> 1 year </p>\n					<p style="padding-bottom: 10px;"><b>Primary Camera:</b>5MP</p>\n					<p style="padding-bottom: 10px;"><b> Secondary Camera:</b> 8MP </p>\n					<p style="padding-bottom: 10px;"><b> Processor: </b> quadcore </p>\n				</ion-col>\n			</ion-row>\n			<ion-row>\n				<ion-col col-md-12>\n					<h1 style="padding-bottom: 10px;">Special Offer</h1>\n					<p style="padding-bottom: 10px;">Get 250 Rs.Cashback on online payment</p>\n				</ion-col>\n			</ion-row>\n			<ion-row>\n				<button ion-button class="cart">Add to Cart</button>\n			</ion-row>\n		</ion-card-content>\n	</ion-card>\n</ion-content>'/*ion-inline-end:"D:\suhrit\suhrit\ionic2\Fairtail\src\pages\productdetails\productdetails.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* MenuController */]])
], ProductdetailsPage);

//# sourceMappingURL=productdetails.js.map

/***/ }),

/***/ 146:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 146;

/***/ }),

/***/ 189:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/productdetails/productdetails.module": [
		585,
		3
	],
	"../pages/tab-page-accessory/tab-page-accessory.module": [
		586,
		2
	],
	"../pages/tabmobile/tabmobile.module": [
		587,
		1
	],
	"../pages/tabpagetablet/tabpagetablet.module": [
		588,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 189;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 234:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MidconnectorPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__productcatsubcat_productcatsubcat__ = __webpack_require__(235);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MidconnectorPage = (function () {
    function MidconnectorPage(navCtrl, menuCtrl) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.menuCtrl.enable(true, 'myMenu');
    }
    MidconnectorPage.prototype.productcatsubcat = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__productcatsubcat_productcatsubcat__["a" /* ProductcatsubcatPage */], {});
    };
    return MidconnectorPage;
}());
MidconnectorPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-midconnector',template:/*ion-inline-start:"D:\suhrit\suhrit\ionic2\Fairtail\src\pages\midconnector\midconnector.html"*/'<ion-header  >\n   <ion-toolbar>\n   <button ion-button menuToggle>\n      <ion-icon name="menu" class="whitecc"></ion-icon>\n    </button>\n    <ion-title ><b class="whitecc"> Infiget India  Pvt. </b> <ion-icon class="whitecc" style="margin-left: 1cm;"  name="cart"></ion-icon></ion-title>\n     \n   </ion-toolbar>\n\n<ion-toolbar>\n<ion-searchbar ></ion-searchbar>\n<ion-list>\n \n</ion-list>\n   </ion-toolbar>\n\n\n</ion-header>\n\n<ion-content>\n\n<ion-card (click)="productcatsubcat()">\n <ion-card-content>\n <ion-row>\n  <ion-col col-6> \n   <img  style="border-radius: 0%;" src="assets/midconnect/products.png" class="custom-avatar"/>\n  </ion-col>\n  <ion-col col-6>\n    <ion-col >\n     <h1><b>Show Products</b></h1>\n     </ion-col>\n  \n    \n  </ion-col>\n</ion-row>\n</ion-card-content>\n</ion-card>\n\n<ion-card (click)="productcatsubcat()">\n <ion-card-content>\n\n <ion-row>\n  <ion-col col-6> \n <img  style="border-radius: 0%;" src="assets/midconnect/offer.png" class="custom-avatar"/>\n  </ion-col>\n  <ion-col col-6>\n    <ion-col >\n     <h1><b>Offer</b></h1>\n     </ion-col>\n  \n  </ion-col>\n</ion-row>\n</ion-card-content>\n</ion-card>\n\n\n\n<ion-card  (click)="productcatsubcat()" >\n <ion-card-content> \n\n <ion-row>\n  <ion-col col-6> \n  <img  style="border-radius: 0%;" src="assets/midconnect/new.png" class="custom-avatar"/>\n  </ion-col>\n  <ion-col col-6>\n    <ion-col >\n     <h1><b>New Launcher</b></h1>\n     </ion-col>\n  </ion-col>\n</ion-row>\n</ion-card-content>\n</ion-card>\n\n\n\n\n<ion-card  (click)="productcatsubcat()">\n <ion-card-content>\n\n <ion-row>\n  <ion-col col-6> \n   <img  style="border-radius: 0%;" src="assets/midconnect/scheme.png" class="custom-avatar"/>\n  </ion-col>\n  <ion-col col-6>\n    <ion-col>\n     <h1><b>Scheme</b></h1>\n     </ion-col>\n    </ion-col>\n</ion-row>\n</ion-card-content>\n</ion-card>\n\n\n<ion-card (click)="productcatsubcat()">\n <ion-card-content>\n\n <ion-row>\n  <ion-col col-6> \n   <img  style="border-radius: 0%;" src="assets/midconnect/heart.png" class="custom-avatar"/>\n  </ion-col>\n  <ion-col col-6>\n    <ion-col>\n     <h1><b>Faviourite</b></h1>\n     </ion-col>\n    </ion-col>\n</ion-row>\n</ion-card-content>\n</ion-card>\n\n\n</ion-content>\n\n'/*ion-inline-end:"D:\suhrit\suhrit\ionic2\Fairtail\src\pages\midconnector\midconnector.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* MenuController */]])
], MidconnectorPage);

//# sourceMappingURL=midconnector.js.map

/***/ }),

/***/ 235:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductcatsubcatPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__productdetails_productdetails__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic2_super_tabs__ = __webpack_require__(236);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ProductcatsubcatPage = (function () {
    function ProductcatsubcatPage(navCtrl, navParams, menuCtrl, toastCtrl, superTabsCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.menuCtrl = menuCtrl;
        this.toastCtrl = toastCtrl;
        this.superTabsCtrl = superTabsCtrl;
        this.page1 = 'TabmobilePage';
        this.page2 = 'TabPageAccessoryPage';
        this.page3 = 'TabPageTabletPage';
        this.showIcons = true;
        this.showTitles = true;
        this.pageTitle = 'Full Height';
        this.menuCtrl.enable(true, 'myMenu');
        var type = navParams.get('type');
        switch (type) {
            case 'icons-only':
                this.showTitles = false;
                this.pageTitle += ' - Icons only';
                break;
            case 'titles-only':
                this.showIcons = false;
                this.pageTitle += ' - Titles only';
                break;
        }
    }
    ProductcatsubcatPage.prototype.midconnector = function () {
        var toast = this.toastCtrl.create({
            message: 'This functionality is Under Construction',
            duration: 3000
        });
        toast.present();
    };
    ProductcatsubcatPage.prototype.productdetails = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__productdetails_productdetails__["a" /* ProductdetailsPage */], {});
    };
    ProductcatsubcatPage.prototype.onTabSelect = function (tab) {
        console.log("Selected tab: ", tab);
    };
    return ProductcatsubcatPage;
}());
ProductcatsubcatPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-productcatsubcat',template:/*ion-inline-start:"D:\suhrit\suhrit\ionic2\Fairtail\src\pages\productcatsubcat\productcatsubcat.html"*/'<ion-header  >\n   <ion-toolbar>\n   <button ion-button menuToggle>\n      <ion-icon name="menu" class="whitecc"></ion-icon>\n    </button>\n    <ion-title ><b class="whitecc"> Infiget India  Pvt. </b> <ion-icon class="whitecc" style="margin-left: 1cm;"  name="cart"></ion-icon></ion-title>\n     \n   </ion-toolbar>\n\n<ion-toolbar>\n<ion-searchbar ></ion-searchbar>\n<ion-list>\n \n</ion-list>\n </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n  <super-tabs style="height: 80%" [config]="{ sideMenu: \'left\' }" [toolbarColor]="\'primary\'" indicatorColor="light" badgeColor="light">\n    <super-tab [root]="page1" [title]="showTitles? \'Mobile\' : \'\'" [icon]="showIcons? \'mobile\': \'\'"></super-tab>\n    <super-tab [root]="page2" [title]="showTitles? \'Accessories\' : \'\'" [icon]="showIcons? \'accessory\' : \'\'"></super-tab>\n    <super-tab [root]="page3" [title]="showTitles? \'Tablet\' : \'\'" [icon]="showIcons? \'tab\' : \'\'"></super-tab>\n  </super-tabs>\n\n\n</ion-content>\n\n'/*ion-inline-end:"D:\suhrit\suhrit\ionic2\Fairtail\src\pages\productcatsubcat\productcatsubcat.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* MenuController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ToastController */], __WEBPACK_IMPORTED_MODULE_3_ionic2_super_tabs__["a" /* SuperTabsController */]])
], ProductcatsubcatPage);

//# sourceMappingURL=productcatsubcat.js.map

/***/ }),

/***/ 244:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SplashscreenPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Rx__ = __webpack_require__(325);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__list_list__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__home_home__ = __webpack_require__(134);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var SplashscreenPage = (function () {
    function SplashscreenPage(navCtrl, storage, toastCtrl, menuCtrl) {
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.toastCtrl = toastCtrl;
        this.menuCtrl = menuCtrl;
        this.menuCtrl.enable(false, 'myMenu');
    }
    SplashscreenPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.storage.get('logindata').then(function (logindata) {
            __WEBPACK_IMPORTED_MODULE_4_rxjs_Rx__["Observable"].timer(22 * 60).subscribe(function (x) {
                if (logindata !== null) {
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__list_list__["a" /* ListPage */], {});
                }
                else {
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__home_home__["a" /* HomePage */], {});
                }
            });
        });
    };
    return SplashscreenPage;
}());
SplashscreenPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-splashscreen',template:/*ion-inline-start:"D:\suhrit\suhrit\ionic2\Fairtail\src\pages\splashscreen\splashscreen.html"*/'<ion-header>\n\n</ion-header>\n\n\n<ion-content padding class="masters">\n\n <ion-item no-lines class="masters">\n    <ion-avatar item-left>\n    </ion-avatar>\n  </ion-item>\n\n<ion-item no-lines class="masters">\n	<ion-avatar item-left>\n	</ion-avatar>\n</ion-item>\n\n<ion-item no-lines class="masters">\n    <ion-avatar item-left>\n    </ion-avatar>\n </ion-item>\n\n<ion-item no-lines class="masters">\n    <ion-avatar item-left>\n    </ion-avatar>\n </ion-item>\n <marquee behavior="scroll" behavior="alternate" scrollamount="2" class ="frontsize">Infiget</marquee>\n\n\n   \n\n</ion-content>\n'/*ion-inline-end:"D:\suhrit\suhrit\ionic2\Fairtail\src\pages\splashscreen\splashscreen.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* MenuController */]])
], SplashscreenPage);

//# sourceMappingURL=splashscreen.js.map

/***/ }),

/***/ 267:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RetailerSignup; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__home_home__ = __webpack_require__(134);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var RetailerSignup = (function () {
    function RetailerSignup(navCtrl, menuCtrl, http, toastCtrl) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.http = http;
        this.toastCtrl = toastCtrl;
        this.menuCtrl.enable(false, 'myMenu');
        this.initializeState();
        //this.initializeDistrict();
        this.initializeCity();
        this.initializeRetailerType();
    }
    RetailerSignup.prototype.initializeRetailerType = function () {
        this.retailer_type = [
            { id: 1, name: 'Retailer 1' },
            { id: 2, name: 'Retailer 2' },
            { id: 3, name: 'Retailer 3' }
        ];
    };
    RetailerSignup.prototype.initializeState = function () {
        this.states = [
            { id: 1, name: 'Delhi' },
            { id: 2, name: 'West Bengal' },
            { id: 3, name: 'Maharastra' }
        ];
    };
    RetailerSignup.prototype.initializeCity = function () {
        this.cities = [
            { id: 1, name: 'Delhi Cantonment', state_id: 1 },
            { id: 2, name: 'Fatehpur Beri', state_id: 1 },
            { id: 3, name: 'Kolkata', state_id: 2 },
            { id: 4, name: 'Asansol', state_id: 2 },
            { id: 5, name: 'North Bengal', state_id: 2 },
            { id: 6, name: 'Mumbai', state_id: 3 },
            { id: 7, name: 'Pune', state_id: 3 }
        ];
    };
    RetailerSignup.prototype.setDistrictValues = function (sState) {
        this.selectedDistricts = this.districts.filter(function (district) { return district.state_id == sState.id; });
    };
    RetailerSignup.prototype.setCityValues = function (sState) {
        this.selectedCities = this.cities.filter(function (city) { return city.state_id == sState.id; });
    };
    RetailerSignup.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    RetailerSignup.prototype.doRegister = function () {
        var _this = this;
        if (this.firstname == null || this.firstname == "") {
            var toast = this.toastCtrl.create({
                message: 'Please fill a the Firstname',
                duration: 3000
            });
            toast.present();
        }
        else if (this.lastname == null || this.lastname == "") {
            var toast = this.toastCtrl.create({
                message: 'Please fill the Lastname',
                duration: 3000
            });
            toast.present();
        }
        else if (this.company_name == null || this.company_name == "") {
            var toast = this.toastCtrl.create({
                message: 'Please fill  the Company Name',
                duration: 3000
            });
            toast.present();
        }
        else if (this.email == null || this.email == "") {
            var toast = this.toastCtrl.create({
                message: 'Please fill Email',
                duration: 3000
            });
            toast.present();
        }
        else if (this.username == null || this.username == "") {
            var toast = this.toastCtrl.create({
                message: 'Please fill Username',
                duration: 3000
            });
            toast.present();
        }
        else if (this.password == null || this.password == "") {
            var toast = this.toastCtrl.create({
                message: 'Please fill Password',
                duration: 3000
            });
            toast.present();
        }
        else if (this.address1 == null || this.address1 == "") {
            var toast = this.toastCtrl.create({
                message: 'Please fill Address1',
                duration: 3000
            });
            toast.present();
        }
        else if (this.address2 == null || this.address2 == "") {
            var toast = this.toastCtrl.create({
                message: 'Please fill  the Address2',
                duration: 3000
            });
            toast.present();
        }
        else if (this.area == null || this.area == "") {
            var toast = this.toastCtrl.create({
                message: 'Please fill all the Fields',
                duration: 3000
            });
            toast.present();
        }
        else if (this.pincode == null || this.pincode == "") {
            var toast = this.toastCtrl.create({
                message: 'Please fill  the Pincode',
                duration: 3000
            });
            toast.present();
        }
        else if (this.unique_code == null || this.unique_code == "") {
            var toast = this.toastCtrl.create({
                message: 'Please fill Unique',
                duration: 3000
            });
            toast.present();
        }
        else {
            var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
            headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
            var options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers });
            //alert(this.sRetailerType.name);
            var reqParam = {
                first_name: this.firstname,
                last_name: this.lastname,
                companyfirm: this.company_name,
                user_type: this.sRetailerType.id,
                email: this.email,
                user_name: this.username,
                password: this.password,
                address1: this.address1,
                address2: this.address2,
                area_zone: this.area,
                city: this.sCity.id,
                state: this.sState.id,
                pincode: this.pincode,
                distributor_code: this.unique_code
            };
            console.log("response == " + JSON.stringify(reqParam));
            this.http.post("http://infiget.com/retailerwebservice/retailersignup", JSON.stringify(reqParam), options)
                .subscribe(function (data) {
                console.log(data['_body']);
                var response = JSON.parse(data['_body']);
                console.log(response.success);
                var success = response.success;
                if (success == 1) {
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */], {});
                    var toast = _this.toastCtrl.create({
                        message: 'Successfully Register : wait for Admin cofirmation',
                        duration: 3000
                    });
                    toast.present();
                }
                else {
                    var toast = _this.toastCtrl.create({
                        message: 'Sorry! Unable to Register',
                        duration: 3000
                    });
                    toast.present();
                }
            }, function (error) {
                console.log(error); // Error getting the data
            });
        }
    };
    return RetailerSignup;
}());
RetailerSignup = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-retailersignup',template:/*ion-inline-start:"D:\suhrit\suhrit\ionic2\Fairtail\src\pages\retailersignup\retailersignup.html"*/'\n<ion-header >\n  <ion-navbar class="masters">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title><b class="whitecc"> Registration </b></ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding class="masters">\n\n\n        <ion-item class="masters">\n          <ion-label floating><b class="whitecc"> First Name </b></ion-label>\n          <ion-input type="text" class="inputfield" [(ngModel)]="firstname" ></ion-input>\n        </ion-item>\n   \n         <ion-item class="masters">\n          <ion-label floating><b class="whitecc"> Last Name </b></ion-label>\n          <ion-input type="text" class="inputfield" [(ngModel)]="lastname"></ion-input>\n      </ion-item>\n  \n         <ion-item class="masters">\n          <ion-label floating><b class="whitecc"> Comapny/Firm Name  </b></ion-label>\n          <ion-input type="text" class="inputfield" [(ngModel)]="company_name"></ion-input>\n        </ion-item>\n      \n         <ion-item class="masters">  \n          <ion-label floating><b class="whitecc"> Username </b></ion-label>\n          <ion-input type="text" class="inputfield" [(ngModel)]="username"></ion-input>\n        </ion-item>  \n   \n         <!-- <ion-item class="masters">\n          <ion-label floating><b class="whitecc"> Retailer Type </b></ion-label>\n          <ion-input type="text" class="inputfield" [(ngModel)]="type"></ion-input>\n                 </ion-item> -->\n\n        <ion-item class="masters">\n          <ion-label floating><b class="whitecc"> Retailer Type </b></ion-label>  \n          <ion-select [(ngModel)]="sRetailerType" class="inputfield" interface="action-sheet">\n            <ion-option [value]="sRetailerType" *ngFor = "let sRetailerType of retailer_type">{{sRetailerType.name}}</ion-option>\n        </ion-select> \n\n        </ion-item> \n        <ion-item class="masters">\n          <ion-label floating><b class="whitecc"> Distributor Code </b></ion-label>\n          <ion-input type="text" class="inputfield" [(ngModel)]="unique_code"></ion-input>\n        </ion-item>\n\n        <ion-item class="masters">\n          <ion-label floating><b class="whitecc"> Contact Person Name </b></ion-label>\n          <ion-input type="text" class="inputfield" [(ngModel)]="contact_name"></ion-input>\n        </ion-item>\n\n        <ion-item class="masters">\n          <ion-label floating><b class="whitecc"> Contact Person Mobile </b></ion-label>\n          <ion-input type="number" class="inputfield" [(ngModel)]="contact_phone"></ion-input>\n        </ion-item>\n\n        <ion-item class="masters">\n          <ion-label floating><b class="whitecc"> Password </b></ion-label>\n          <ion-input type="password" class="inputfield" [(ngModel)]="password"></ion-input>\n        </ion-item>\n\n        <ion-item class="masters">\n          <ion-label floating><b class="whitecc"> Email Address </b></ion-label>\n          <ion-input type="email" class="inputfield" [(ngModel)]="email"></ion-input>\n        </ion-item>\n\n        <ion-item class="masters">\n          <ion-label floating><b class="whitecc"> Address 1 </b></ion-label>\n          <ion-input type="text" class="inputfield" [(ngModel)]="address1"></ion-input>\n        </ion-item>\n\n        <ion-item class="masters">\n          <ion-label floating><b class="whitecc"> Address 2 </b></ion-label>\n          <ion-input type="text" class="inputfield" [(ngModel)]="address2"></ion-input>\n        </ion-item>\n\n        <ion-item class="masters">\n          <ion-label floating><b class="whitecc"> Area/Zone </b></ion-label>\n          <ion-input type="text" class="inputfield" [(ngModel)]="area"></ion-input>\n        </ion-item>\n\n        <ion-item class="masters">\n          <ion-label floating><b class="whitecc"> State </b></ion-label>  \n          <ion-select (ionChange)="setCityValues(sState)" [(ngModel)]="sState" class="inputfield" interface="action-sheet">\n            <ion-option [value]="sState" *ngFor = "let sState of states">{{sState.name}}</ion-option>\n          </ion-select> \n        </ion-item>     \n<!--  checked="{{item.checked}}" -->    \n\n        <ion-list *ngIf="selectedCities">\n          <ion-item class="masters">\n            <ion-label floating><b class="whitecc"> City </b></ion-label>  \n            <ion-select class="masters" [(ngModel)]="sCity" interface="action-sheet" class="inputfield">\n              <ion-option [value]="city" *ngFor = "let city of selectedCities">{{city.name}}</ion-option>\n            </ion-select>   \n          </ion-item> \n        </ion-list>  \n\n\n        <!-- <ion-item>\n             <ion-label>State</ion-label>\n             <ion-select (ionChange)="setDistrictValues(sState)" [(ngModel)]="sState">\n          <ion-option [value]="sState" *ngFor = "let sState of states">{{sState.name}}  </ion-option> \n            </ion-select>\n          \n         </ion-item>\n        <ion-item *ngIf="selectedDistricts">\n              <ion-label>District</ion-label>\n              <ion-select (ionChange)="setCityValues(sDistrict)" [(ngModel)]="sDistrict">\n            <ion-option [value]="sDistrict" *ngFor = "let sDistrict of selectedDistricts">{{sDistrict.name}}</ion-option>\n              </ion-select>\n         </ion-item>\n        <ion-list *ngIf="selectedCities">\n              <ion-item *ngFor="let city of selectedCities">\n            <p>{{city.name}}</p>  \n              </ion-item>\n        </ion-list> -->\n\n\n        <ion-item class="masters">\n          <ion-label floating><b class="whitecc"> Pincode </b></ion-label>\n          <ion-input type="text" class="inputfield" [(ngModel)]="pincode"></ion-input>\n        </ion-item>\n\n\n       <br>\n       <button ion-button  full  color="secondary" (click)="doRegister()" class="loginbutton" >Register Me</button>\n\n      <ion-item no-lines class="masters">\n      <ion-row>\n        <button ion-button class="txtcentre" (click)="goBack()"  clear small>Already Register</button>\n      </ion-row>\n\n       </ion-item>\n\n  </ion-content>\n'/*ion-inline-end:"D:\suhrit\suhrit\ionic2\Fairtail\src\pages\retailersignup\retailersignup.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* MenuController */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ToastController */]])
], RetailerSignup);

//# sourceMappingURL=retailersignup.js.map

/***/ }),

/***/ 268:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(269);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(273);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 273:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_data__ = __webpack_require__(313);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__(314);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_home_home__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_list_list__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_retailersignup_retailersignup__ = __webpack_require__(267);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_splashscreen_splashscreen__ = __webpack_require__(244);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_midconnector_midconnector__ = __webpack_require__(234);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_productcatsubcat_productcatsubcat__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_productdetails_productdetails__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_status_bar__ = __webpack_require__(229);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_splash_screen__ = __webpack_require__(233);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_ionic2_super_tabs__ = __webpack_require__(236);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

















var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_8__pages_list_list__["a" /* ListPage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_retailersignup_retailersignup__["a" /* RetailerSignup */],
            __WEBPACK_IMPORTED_MODULE_10__pages_splashscreen_splashscreen__["a" /* SplashscreenPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_midconnector_midconnector__["a" /* MidconnectorPage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_productcatsubcat_productcatsubcat__["a" /* ProductcatsubcatPage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_productdetails_productdetails__["a" /* ProductdetailsPage */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */], {}, {
                links: [
                    { loadChildren: '../pages/productdetails/productdetails.module#ProductdetailsPageModule', name: 'ProductdetailsPage', segment: 'productdetails', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/tab-page-accessory/tab-page-accessory.module#TabPageAccessoryPageModule', name: 'TabPageAccessoryPage', segment: 'tab-page-accessory', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/tabmobile/tabmobile.module#TabmobilePageModule', name: 'TabmobilePage', segment: 'tabmobile', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/tabpagetablet/tabpagetablet.module#TabPageTabletPageModule', name: 'TabPageTabletPage', segment: 'tabpagetablet', priority: 'low', defaultHistory: [] }
                ]
            }),
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_16_ionic2_super_tabs__["b" /* SuperTabsModule */].forRoot()
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_8__pages_list_list__["a" /* ListPage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_retailersignup_retailersignup__["a" /* RetailerSignup */],
            __WEBPACK_IMPORTED_MODULE_10__pages_splashscreen_splashscreen__["a" /* SplashscreenPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_midconnector_midconnector__["a" /* MidconnectorPage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_productcatsubcat_productcatsubcat__["a" /* ProductcatsubcatPage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_productdetails_productdetails__["a" /* ProductdetailsPage */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_5__providers_data__["a" /* Data */],
            __WEBPACK_IMPORTED_MODULE_14__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_15__ionic_native_splash_screen__["a" /* SplashScreen */],
            { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* IonicErrorHandler */] }
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 313:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Data; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_storage__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var Data = (function () {
    function Data(storage) {
        this.storage = storage;
    }
    Data.prototype.getData = function () {
        return this.storage.get('todos');
    };
    Data.prototype.save = function (data) {
        var newData = JSON.stringify(data);
        this.storage.set('todos', newData);
    };
    Data.prototype.saveBrandItems = function (dataItems) {
        var newData = dataItems;
    };
    return Data;
}());
Data = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__ionic_storage__["b" /* Storage */]])
], Data);

//# sourceMappingURL=data.js.map

/***/ }),

/***/ 314:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(229);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(233);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_list_list__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_splashscreen_splashscreen__ = __webpack_require__(244);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen, storage) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.storage = storage;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_6__pages_splashscreen_splashscreen__["a" /* SplashscreenPage */];
        this.initializeApp();
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Home', component: __WEBPACK_IMPORTED_MODULE_5__pages_list_list__["a" /* ListPage */] },
            { title: 'Log Out', component: null }
        ];
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        if (page.component) {
            this.nav.setRoot(page.component);
        }
        else {
            this.storage.set('logindata', null);
            this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_6__pages_splashscreen_splashscreen__["a" /* SplashscreenPage */]);
        }
    };
    return MyApp;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_14" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Nav */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Nav */])
], MyApp.prototype, "nav", void 0);
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"D:\suhrit\suhrit\ionic2\Fairtail\src\app\app.html"*/'<ion-menu id="myMenu" [content]="content">\n  <ion-header>\n    <ion-toolbar>\n     \n\n\n  <ion-card text-center class="hide-card">\n    <img src="assets/user-icon.png" class="custom-avatar"/>\n     <br>\n    <p> <b>Welcome,Retailer</b></p>\n    <hr>\n</ion-card>\n \n\n\n\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content>\n    <ion-list no-lines>\n      <button menuClose style="margin-left: 2cm;"   ion-item *ngFor="let p of pages" (click)="openPage(p)">\n        {{p.title}}\n      </button>\n    </ion-list>\n\n </ion-content>\n\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"D:\suhrit\suhrit\ionic2\Fairtail\src\app\app.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 74:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__midconnector_midconnector__ = __webpack_require__(234);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ListPage = (function () {
    function ListPage(navCtrl, menuCtrl, alertCtrl, http) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.alertCtrl = alertCtrl;
        this.http = http;
        this.items = [];
        this.brandItem = [];
        this.menuCtrl.enable(true, 'myMenu');
        this.fetchBrand();
    }
    ListPage.prototype.fetchBrand = function () {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        var options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers });
        var postParams = {
            data: "",
        };
        this.http.post("http://infiget.com/api/api/categorylisting", JSON.stringify(postParams), options)
            .subscribe(function (data) {
            var response = JSON.parse(data['_body']);
            console.log("response " + response.success);
            if (response.success == 1) {
                //this.items = JSON.parse(todos); 
                console.log("RESULT == " + response.result[0].code);
                _this.items = response.result;
                for (var i = 0; i < response.result.length; i++) {
                    for (var j = 0; j < response.result[i].brands.length; j++) {
                        console.log(response.result[i].brands[j]);
                        //brandItem = response.result[i].brands[j];
                    }
                }
                //this.dataService.saveBrandItems();
            }
            else {
            }
        }, function (error) {
            console.log(error); // Error getting the data
        });
    };
    ListPage.prototype.midconnector = function (brands) {
        console.log("brand id " + brands.id);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__midconnector_midconnector__["a" /* MidconnectorPage */], {});
    };
    return ListPage;
}());
ListPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-list',template:/*ion-inline-start:"D:\suhrit\suhrit\ionic2\Fairtail\src\pages\list\list.html"*/'<ion-header  >\n   <ion-toolbar>\n   <button ion-button menuToggle>\n      <ion-icon name="menu" class="whitecc"></ion-icon>\n    </button>\n    <ion-title ><b class="whitecc"> Infiget India  Pvt. </b> <ion-icon class="whitecc" style="margin-left: 1cm;"  name="cart"></ion-icon></ion-title>\n     \n   </ion-toolbar>\n\n<ion-toolbar>\n<ion-searchbar ></ion-searchbar>\n<ion-list>\n \n</ion-list>\n   </ion-toolbar>\n\n\n</ion-header>\n\n<ion-content>\n\n<ion-slides style="max-height:200px"  autoplay="3000" loop="true" speed="1000">\n  <ion-slide>\n    <img style="height:200px;" src="assets/slider/img1.jpg"/>\n  </ion-slide>\n  <ion-slide>\n    <img style="height:200px;" src="assets/slider/img2.jpg"/>\n  </ion-slide>\n  <ion-slide>\n    <img style="height:200px;" src="assets/slider/img3.jpg"/>\n  </ion-slide>\n</ion-slides>\n\n<ion-list *ngFor="let item of items">\n<!-- <h3>{{item.name}}</h3> -->\n<ion-card *ngFor="let brands of item[\'brands\']" (click)="midconnector(brands)">\n  <ion-card-content>\n    <ion-row>\n      <ion-col col-6> \n        <img  style="border-radius: 0%;" src="{{brands.image}}" class="custom-avatar"/>\n      </ion-col>\n      <!-- assets/mobile/honor_8_pro_screen_small_1497260751475.jpg -->\n      <ion-col col-6>\n        <ion-col >\n          <h1><b>{{brands.name}}</b></h1>\n        </ion-col>\n        <!-- <ion-col >\n          <p> {{brands.name}}</p>\n        </ion-col> -->\n      </ion-col>\n    </ion-row>\n  </ion-card-content>\n</ion-card>\n</ion-list>\n<!-- <ion-card (click)="midconnector()">\n <ion-card-content>\n\n <ion-row>\n  <ion-col col-6> \n   <img  style="border-radius: 0%;" src="assets/latest/lenovo.png" class="custom-avatar"/>\n  </ion-col>\n  <ion-col col-6>\n    <ion-col >\n     <h1><b>Lenevo</b></h1>\n     </ion-col>\n  \n     <ion-col >\n    <p> Tara Mobile Center </p>\n     </ion-col>\n  </ion-col>\n</ion-row>\n</ion-card-content>\n</ion-card>\n\n\n\n<ion-card (click)="midconnector()">\n <ion-card-content> \n\n <ion-row>\n  <ion-col col-6> \n <img  style="border-radius: 0%;" src="assets/latest/galaxy-devices.png" class="custom-avatar"/>\n  </ion-col>\n  <ion-col col-6>\n    <ion-col >\n     <h1><b>Samsung</b></h1>\n     </ion-col>\n  \n     <ion-col >\n    <p  > Tara Mobile Center </p>\n     </ion-col>\n  </ion-col>\n</ion-row>\n</ion-card-content>\n</ion-card>\n\n\n\n\n<ion-card (click)="midconnector()">\n <ion-card-content>\n\n <ion-row>\n  <ion-col col-6> \n   <img  style="border-radius: 0%;" src="assets/latest/lenovo.png" class="custom-avatar"/>\n  </ion-col>\n  <ion-col col-6>\n    <ion-col>\n     <h1><b>Lenevo</b></h1>\n     </ion-col>\n  \n     <ion-col >\n    <p>  Yukti Mobile Store </p>\n     </ion-col>\n  </ion-col>\n</ion-row>\n</ion-card-content>\n</ion-card> -->\n\n\n\n\n\n\n\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"D:\suhrit\suhrit\ionic2\Fairtail\src\pages\list\list.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* MenuController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]])
], ListPage);

//# sourceMappingURL=list.js.map

/***/ })

},[268]);
//# sourceMappingURL=main.js.map