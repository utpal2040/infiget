webpackJsonp([1],{

/***/ 295:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabmobilePageModule", function() { return TabmobilePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tabmobile__ = __webpack_require__(298);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var TabmobilePageModule = (function () {
    function TabmobilePageModule() {
    }
    return TabmobilePageModule;
}());
TabmobilePageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__tabmobile__["a" /* TabmobilePage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__tabmobile__["a" /* TabmobilePage */]),
        ],
    })
], TabmobilePageModule);

//# sourceMappingURL=tabmobile.module.js.map

/***/ }),

/***/ 298:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabmobilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__productdetails_productdetails__ = __webpack_require__(212);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the TabmobilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TabmobilePage = (function () {
    function TabmobilePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    TabmobilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TabmobilePage');
    };
    TabmobilePage.prototype.productdetails = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__productdetails_productdetails__["a" /* ProductdetailsPage */], {});
    };
    return TabmobilePage;
}());
TabmobilePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-tabmobile',template:/*ion-inline-start:"D:\suhrit\suhrit\ionic2\Fairtail\src\pages\tabmobile\tabmobile.html"*/'<!--\n\n  Generated template for the TabmobilePage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<!-- <ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>tabmobile</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header> -->\n\n\n\n\n\n<ion-content padding>\n\n<ion-card (click)="productdetails()">\n\n      <ion-card-content>\n\n        <ion-row>\n\n          <ion-col col-6> \n\n            <img  style="border-radius: 0%;" src="assets/mobile/honor_8_pro_screen_small_1497260751475.jpg" class="custom-avatar"/>\n\n          </ion-col>\n\n          <ion-col col-6>\n\n            <ion-col >\n\n              <h1><b>Apple</b></h1>\n\n            </ion-col>\n\n  \n\n            <ion-col >\n\n              <p> The Mobile Store</p>\n\n            </ion-col>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-card-content>\n\n    </ion-card>\n\n	\n\n  <!-- <ion-list> \n\n  \n\n  \n\n  \n\n  \n\n    <ion-card (click)="productdetails()">\n\n      <ion-card-content>\n\n        <ion-row>\n\n          <ion-col col-6> \n\n            <img  style="border-radius: 0%;" src="assets/mobile/honor_8_pro_screen_small_1497260751475.jpg" class="custom-avatar"/>\n\n          </ion-col>\n\n          <ion-col col-6>\n\n            <ion-col >\n\n              <h1><b>Apple</b></h1>\n\n            </ion-col>\n\n  \n\n            <ion-col >\n\n              <p> The Mobile Store</p>\n\n            </ion-col>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-card-content>\n\n    </ion-card>\n\n  \n\n    <ion-card (click)="midconnector()">\n\n      <ion-card-content>\n\n  \n\n        <ion-row>\n\n          <ion-col col-6> \n\n            <img  style="border-radius: 0%;" src="assets/latest/lenovo.png" class="custom-avatar"/>\n\n          </ion-col>\n\n          <ion-col col-6>\n\n            <ion-col >\n\n              <h1><b>Lenevo</b></h1>\n\n            </ion-col>\n\n  \n\n            <ion-col >\n\n              <p> Tara Mobile Center </p>\n\n            </ion-col>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-card-content>\n\n    </ion-card>\n\n  \n\n  \n\n  \n\n    <ion-card (click)="midconnector()">\n\n      <ion-card-content> \n\n  \n\n        <ion-row>\n\n          <ion-col col-6> \n\n            <img  style="border-radius: 0%;" src="assets/latest/galaxy-devices.png" class="custom-avatar"/>\n\n          </ion-col>\n\n          <ion-col col-6>\n\n            <ion-col >\n\n              <h1><b>Samsung</b></h1>\n\n            </ion-col>\n\n  \n\n            <ion-col >\n\n              <p  > Tara Mobile Center </p>\n\n            </ion-col>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-card-content>\n\n    </ion-card>\n\n  \n\n  \n\n  \n\n  \n\n    <ion-card (click)="midconnector()">\n\n      <ion-card-content>\n\n  \n\n        <ion-row>\n\n          <ion-col col-6> \n\n            <img  style="border-radius: 0%;" src="assets/latest/lenovo.png" class="custom-avatar"/>\n\n          </ion-col>\n\n          <ion-col col-6>\n\n            <ion-col>\n\n              <h1><b>Lenevo</b></h1>\n\n            </ion-col>\n\n  \n\n            <ion-col >\n\n              <p>  Yukti Mobile Store </p>\n\n            </ion-col>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-card-content>\n\n    </ion-card>\n\n  \n\n  </ion-list> -->\n\n\n\n  dghfdghfgh\n\n</ion-content>\n\n'/*ion-inline-end:"D:\suhrit\suhrit\ionic2\Fairtail\src\pages\tabmobile\tabmobile.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */]])
], TabmobilePage);

//# sourceMappingURL=tabmobile.js.map

/***/ })

});
//# sourceMappingURL=1.js.map